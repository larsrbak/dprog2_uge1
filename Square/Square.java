import java.util.*;

public class Square {

	public static void main(String[] args) {
		if(args.length == 1) {
			int size = Integer.parseInt(args[0]);
			int[] array = new int[size];
			Scanner in = new Scanner(System.in);
			for (int i = 0; i < size; i++) {
				System.out.print("Indtast tal nummer " + (i+1) + ": ");
				array[i] = in.nextInt();
			}
			System.out.print("Kvadraterne på de indtastede tal er:"); // Hvordan får jeg den til at udskrive æøå?
			for (int i : array) {
				System.out.print(" " + (int) Math.pow(i, 2));
			}
			System.out.println(); // Kun en design mæssig ting
		}
		else  {
			System.out.println("Skriv kun et heltal");
		}
	}
}
