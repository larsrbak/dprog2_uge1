public class AddressBookTest {
	public static void main(String[] args) {
		if(args == null || args.length == 0 || args.length <= 2) {
			System.out.println();
			System.out.println("Please reenter the command with an string");
			System.out.println("The string can ether be \"-g\" for graphical userinterface or \"-t\" for textmode");
			System.out.println();
		}
		else if (args[0].equals("-g")) {
			AddressBookDemoGUI obj = new AddressBookDemoGUI();
			obj.showInterface();
		}
		else if (args[0].equals("-t")) {
			AddressBookDemoText obj = new AddressBookDemoText();
			obj.showInterface();
		}
	}
}