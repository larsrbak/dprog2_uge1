import java.util.Scanner;

public class Test2 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    System.out.print("Nu skal der adderes! Skriv det første tal: ");
    int nr1 = in.nextInt();
    System.out.print("Skriv nu det tal som du vil lægge til: ");
    int nr2 = in.nextInt();
    System.out.println("Summen af de tal du har angivet er: " + (nr1 + nr2));
  }
}